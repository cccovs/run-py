import os
import sys
import time
import shlex
import subprocess
import threading
import tkinter as tk
from tkinter import ttk, filedialog, messagebox

__VERSION__ = 'v1.2.1'
__LAST_UPDATE__ = '2023-03-07'

class MainForm:
    def __init__(self):
        self.root = tk.Tk()
        self.root.title(f'运行py文件 {__VERSION__}')
        self.root.geometry('{}x{}+{}+{}'.format(450, 250, self.root.winfo_screenwidth()//2-225, self.root.winfo_screenheight()//2-250))
        self.root.resizable(width=False, height=False)
        self.root.attributes('-topmost', 1)
        self.root.protocol('WM_DELETE_WINDOW', self.close_window_event)

        # python解释器选择
        self.entry_1_content = tk.StringVar()
        self.entry_1_content.set('"不指定则使用系统默认的解释器"')
        self.frame_1 = ttk.Frame(self.root)
        self.label_1 = ttk.Label(self.frame_1, text='python解释器', font=('微软雅黑', 10))
        self.entry_1 = ttk.Entry(self.frame_1, textvariable=self.entry_1_content, width=30, font=('微软雅黑', 9))
        self.button_1 = ttk.Button(self.frame_1, text='...', width=2, command=self.choose_python_event)

        self.check_button_var = tk.BooleanVar()
        self.check_button = ttk.Checkbutton(self.frame_1, text='显示命令行', onvalue=True, offvalue=False, variable=self.check_button_var)
        self.check_button_var.set(True)

        self.label_1.grid(row=0, column=0, padx=(5, 10))
        self.entry_1.grid(row=0, column=1)
        self.button_1.grid(row=0, column=2, padx=(10, 5))
        self.check_button.grid(row=0, column=3)
        
        # python源代码选择
        self.entry_2_content = tk.StringVar()
        self.frame_2 = ttk.Frame(self.root)
        self.label_2 = ttk.Label(self.frame_2, text='python源代码', font=('微软雅黑', 10))
        self.entry_2 = ttk.Entry(self.frame_2, textvariable=self.entry_2_content, width=30, font=('微软雅黑', 9))
        self.button_2 = ttk.Button(self.frame_2, text='...', width=2, command=self.choose_code_event)
        self.label_2_2_content = tk.StringVar()
        self.label_2_2 = ttk.Label(self.frame_2, textvariable=self.label_2_2_content, font=('微软雅黑', 8, 'underline'), justify='right', width=20)
        self.label_2.grid(row=0, column=0, padx=(5, 10))
        self.entry_2.grid(row=0, column=1)
        self.button_2.grid(row=0, column=2, padx=(10, 5))
        self.label_2_2.grid(row=0, column=3)
        
        self.frame_1.place(x=0, y=10)
        self.frame_2.place(x=0, y=50)

        # 启动按钮
        self.start_button = ttk.Button(self.root, text='启动', cursor='hand2', command=self.start_event)
        self.start_button.pack(pady=(90, 0))
        
        # 下方状态栏 
        self.state_text = tk.Text(self.root, font=('微软雅黑', 10), foreground='#ff0000')
        self.state_text.pack(side='bottom', anchor='sw',  pady=(10, 20), fill='both', expand=True)
 
        self.sbar = ttk.Scrollbar(self.state_text, orient='vertical', cursor='arrow')
        self.sbar.pack(side='right', fill='y')

        self.state_text.config(yscrollcommand=self.sbar.set)
        self.sbar.config(command=self.state_text.yview)
   
        self.root.mainloop()

    def close_window_event(self):
        if self.start_button.instate((tk.DISABLED, )):
            if messagebox.askokcancel(title='强制关闭窗口提醒', message='{}确定要强制关闭窗口吗?'.format('脚本正在运行中\n'*3)):
                    self.res_cmd.terminate()
                    self.res_cmd.wait()
                    self.root.destroy()
            else:
                return
        else:
            self.root.destroy()

    def choose_python_event(self):
        match sys.platform:
            case 'win32':
                filename = filedialog.askopenfilename(title='选择Python解释器', filetypes=(('Python解释器', 'python.exe'),))
            case 'linux':
                filename = filedialog.askopenfilename(title='选择Python解释器', filetypes=(('Python解释器', 'python'),))
            case _:
                self.entry_1_content.set('无法识别的系统')
                return
        self.entry_1_content.set(filename)

    def choose_code_event(self):
        filename = filedialog.askopenfilename(title='选择Python源代码', filetypes=(('py文件', '*.py'),))
        self.entry_2_content.set(filename)
        if filename:
            self.label_2_2_content.set(os.path.basename(filename)[-10:])
        else:
            self.label_2_2_content.set('用户取消选择')

    def start_event(self):
        self.state_text.delete('0.0', tk.END)
        
        if not os.path.exists(self.entry_2_content.get()) or not self.entry_2_content.get().endswith('.py'):
            self.state_text.insert('0.0', '[{}] {}\n'.format(time.strftime('%H:%M:%S', time.localtime()), '源代码路径设置错误!'))
            return
        else:
            self.state_text.insert('0.0', '[{}] <启动> {}\n'.format(time.strftime('%H:%M:%S', time.localtime()), self.entry_2_content.get()))
            
        match sys.platform:
            case 'win32':
                if self.entry_1_content.get().endswith('python.exe') and os.path.exists(self.entry_1_content.get()):
                    self.start_button.config(state=tk.DISABLED)
                    self.check_button.config(state=tk.DISABLED)
                    threading.Thread(target=self.run, args=(f'{self.entry_1_content.get()} {self.entry_2_content.get()}', self.check_button_var.get()), daemon=True).start()
                else:
                    self.start_button.config(state=tk.DISABLED)
                    self.check_button.config(state=tk.DISABLED)
                    threading.Thread(target=self.run, args=(f'python {self.entry_2_content.get()}', self.check_button_var.get()), daemon=True).start()
            case 'linux':
                if self.entry_1_content.get().endswith('python') and os.path.exists(self.entry_1_content.get()):
                    self.start_button.config(state=tk.DISABLED)
                    self.check_button.config(state=tk.DISABLED)
                    threading.Thread(target=self.run, args=(f'{self.entry_1_content.get()} {self.entry_2_content.get()}', self.check_button_var.get()), daemon=True).start()
                else:
                    self.start_button.config(state=tk.DISABLED)
                    self.check_button.config(state=tk.DISABLED)
                    threading.Thread(target=self.run, args=(f'python3 {self.entry_2_content.get()}', self.check_button_var.get()), daemon=True).start()
            case _:
                self.state_text.insert('0.0', '无法识别的系统\n')
                return   
            
    def run(self, cmd, flag):
        if flag:
            self.res_cmd = subprocess.Popen(shlex.split(cmd), shell=False, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
        else:
            self.res_cmd = subprocess.Popen(shlex.split(cmd), shell=True, stderr=subprocess.STDOUT, stdout=subprocess.PIPE)
            
        self.res_cmd.wait()
        self.state_text.insert('0.0', '[{}] {}\n'.format(time.strftime('%H:%M:%S', time.localtime()), '运行结束'))
        if value := self.res_cmd.stderr:
            self.state_text.insert('0.0', '[{}] {}\n'.format(time.strftime('%H:%M:%S', time.localtime()), value.decode('utf-8')))
        self.start_button.config(state=tk.NORMAL)
        self.check_button.config(state=tk.NORMAL)


if __name__ == '__main__':
    MainForm()