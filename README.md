## 运行py文件
这是一个干净,整洁,轻量级的,方便执行py文件的GUI界面,<br>
支持Windows,Linux

### 样品实例
#### windows下的效果
![win](image/W1.png)

#### linux下的效果
![linux](image/L1.png)

### 更新说明
在v1.1中添加了是否显示命令行的选项(默认显示)
- 开启 可外显print或input语句

### 测试环境
- Windows 11
- Ubuntu 22.04
- Python3.11
